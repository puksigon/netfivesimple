using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AspNetCore.IQueryable.Extensions.Attributes;
using AspNetCore.IQueryable.Extensions.Filter;
using AspNetCore.IQueryable.Extensions.Pagination;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NetFileSimple.Domains.DbModels;
using NetFileSimple.Domains.Interfaces.Repositories;

namespace NetFiveSimple.Application.Mediatr
{
    public class GetProductCommand : IRequest<List<ProductDbModel>>, IQueryPaging
    {
        [QueryOperator(Max = 100)] public int? Limit { get; set; } = 10;
        public int? Offset { get; set; } = 0;
    }
    
    public class GetProductCommandHandler : IRequestHandler<GetProductCommand, List<ProductDbModel>>
    {
        private readonly IProductRepository _productRepository;

        public GetProductCommandHandler(IProductRepository productRepository) // inject repository here
        {
            _productRepository = productRepository;
        }
        public async Task<List<ProductDbModel>> Handle(GetProductCommand request, CancellationToken cancellationToken)
        {
            return await _productRepository.AsQueryable()
                .AsNoTracking()
                .Filter(request)
                .ToListAsync(cancellationToken);
        }
    }
}