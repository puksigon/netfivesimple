using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MapsterMapper;
using MediatR;
using NetFileSimple.Domains.DbModels;
using NetFileSimple.Domains.DTOs;
using NetFileSimple.Domains.Interfaces.Repositories;

namespace NetFiveSimple.Application.Mediatr
{
    public class InsertProductCategoryCommand : IRequest<List<ProductCategoryDbModel>>
    {
        public List<InsertProductCategoryDto> Categories { get; set; }
    }
    
    public class InsertProductCategoryCommandHandler : IRequestHandler<InsertProductCategoryCommand, List<ProductCategoryDbModel>>
    {
        private readonly IMapper _mapper;
        private readonly IProductCategoryRepository _productCategoryRepository;

        public InsertProductCategoryCommandHandler(IMapper mapper, IProductCategoryRepository productCategoryRepository)
        {
            _mapper = mapper;
            _productCategoryRepository = productCategoryRepository;
        }
        public async Task<List<ProductCategoryDbModel>> Handle(InsertProductCategoryCommand request, CancellationToken cancellationToken)
        {
            var requestedInsertProductCategories = _mapper.From(request.Categories).AdaptToType<List<ProductCategoryDbModel>>();
            await _productCategoryRepository.AddRangeAsync(requestedInsertProductCategories, cancellationToken);
            await _productCategoryRepository.SaveChangesAsync(cancellationToken);

            return requestedInsertProductCategories;
        }
    }
}