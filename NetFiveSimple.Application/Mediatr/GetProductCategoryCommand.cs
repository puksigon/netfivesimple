using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AspNetCore.IQueryable.Extensions.Attributes;
using AspNetCore.IQueryable.Extensions.Filter;
using AspNetCore.IQueryable.Extensions.Pagination;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NetFileSimple.Domains.DbModels;
using NetFileSimple.Domains.Interfaces.Repositories;

namespace NetFiveSimple.Application.Mediatr
{
    public class GetProductCategoryCommand : IRequest<List<ProductCategoryDbModel>>, IQueryPaging
    {
        [QueryOperator(Max = 100)] public int? Limit { get; set; } = 10;
        public int? Offset { get; set; } = 0;
    }
    
    public class GetProductCategoryCommandHandler : IRequestHandler<GetProductCategoryCommand, 
        List<ProductCategoryDbModel>>
    {
        private readonly IProductCategoryRepository _productCategoryRepository;

        public GetProductCategoryCommandHandler(IProductCategoryRepository productCategoryRepository)
        {
            _productCategoryRepository = productCategoryRepository;
        }
        public async Task<List<ProductCategoryDbModel>> Handle(GetProductCategoryCommand request, 
            CancellationToken cancellationToken)
        {
            return await _productCategoryRepository.AsQueryable()
                .AsNoTracking()
                .Filter(request)
                .ToListAsync(cancellationToken);
        }
    }
}