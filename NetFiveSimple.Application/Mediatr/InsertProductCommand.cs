using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MapsterMapper;
using MediatR;
using NetFileSimple.Domains.DbModels;
using NetFileSimple.Domains.DTOs;
using NetFileSimple.Domains.Interfaces.Repositories;

namespace NetFiveSimple.Application.Mediatr
{
    public class InsertProductCommand : IRequest<List<ProductDbModel>>
    {
        public List<InsertProductDto> Products { get; set; }
    }
    
    public class InsertProductCommandHandler : IRequestHandler<InsertProductCommand, List<ProductDbModel>>
    {
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;

        public InsertProductCommandHandler(IProductRepository productRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _mapper = mapper;
        }
        public async Task<List<ProductDbModel>> Handle(InsertProductCommand request, CancellationToken cancellationToken)
        {
            var requestedInsertProducts = _mapper.From(request.Products).AdaptToType<List<ProductDbModel>>();
            await _productRepository.AddRangeAsync(requestedInsertProducts, cancellationToken);
            await _productRepository.SaveChangesAsync(cancellationToken);

            return requestedInsertProducts;
        }
    }
}