using System.Reflection;
using Mapster;
using MapsterMapper;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace NetFiveSimple.Application
{
    public static class ServiceRegistration
    {
        public static void AddNetFiveSimpleApplication(this IServiceCollection services, IConfiguration configuration)
        {
            var config = TypeAdapterConfig.GlobalSettings;
            config.Scan(Assembly.GetExecutingAssembly()!);
            config.Default.PreserveReference(true); // circular loop ref.
            services.AddSingleton(config);
            services.AddScoped<IMapper, ServiceMapper>();
            
            services.AddMediatR(Assembly.GetExecutingAssembly());
        }
    }
}