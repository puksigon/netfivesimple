using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NetFiveSimple.Application.Mediatr;

namespace NetFiveSimple.Controllers
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class ProductCategoryController : NetFiveBaseController
    {
        [HttpGet]
        public async Task<IActionResult> GetProductCategories([FromQuery]GetProductCategoryCommand getProductCategoryCommand)
        {
            var result = await Mediator.Send(getProductCategoryCommand);
            return Ok(result);
        }
        
        [HttpPost]
        public async Task<IActionResult> InsertCategories(
            [FromBody] InsertProductCategoryCommand insertProductCategoryCommand)
        {
            var result = await Mediator.Send(insertProductCategoryCommand);
            return Ok(result);
        }
    }
}