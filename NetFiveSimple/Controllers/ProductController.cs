using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NetFiveSimple.Application.Mediatr;

namespace NetFiveSimple.Controllers
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class ProductController : NetFiveBaseController
    {

        [HttpGet]
        public async Task<IActionResult> GetProduct([FromQuery]GetProductCommand getProductCommand)
        {
            var result = await Mediator.Send(getProductCommand);
            return Ok(result);
        }
        
        [HttpPost]
        public async Task<IActionResult> InsertProduct([FromBody] InsertProductCommand insertProductCommand)
        {
            var result = await Mediator.Send(insertProductCommand);
            return Ok(result);
        }
    }
}