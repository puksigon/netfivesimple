using Microsoft.EntityFrameworkCore;

namespace NetFiveSimple.Persistence.DbContext
{
    public class SimpleDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public SimpleDbContext(DbContextOptions<SimpleDbContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
            base.OnModelCreating(modelBuilder);
        }
    }
}