using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NetFileSimple.Domains.Interfaces.Repositories;
using NetFiveSimple.Persistence.DbContext;

namespace NetFiveSimple.Persistence.Repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private readonly SimpleDbContext _simpleDbContext;

        public BaseRepository(SimpleDbContext simpleDbContext)
        {
            _simpleDbContext = simpleDbContext;
        }

        public IQueryable<TEntity> AsQueryable()
        {
            return _simpleDbContext.Set<TEntity>().AsQueryable();
        }

        public async Task AddRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken)
        {
            await _simpleDbContext.Set<TEntity>().AddRangeAsync(entities, cancellationToken);
        }

        public async Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            await _simpleDbContext.SaveChangesAsync(cancellationToken);
        }
    }
}