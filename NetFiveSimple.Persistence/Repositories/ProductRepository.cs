using NetFileSimple.Domains.DbModels;
using NetFileSimple.Domains.Interfaces.Repositories;
using NetFiveSimple.Persistence.DbContext;

namespace NetFiveSimple.Persistence.Repositories
{
    public class ProductRepository : BaseRepository<ProductDbModel>, IProductRepository
    {
        public ProductRepository(SimpleDbContext simpleDbContext) : base(simpleDbContext) { }
    }
}