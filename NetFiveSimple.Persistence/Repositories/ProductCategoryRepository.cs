using NetFileSimple.Domains.DbModels;
using NetFileSimple.Domains.Interfaces.Repositories;
using NetFiveSimple.Persistence.DbContext;

namespace NetFiveSimple.Persistence.Repositories
{
    public class ProductCategoryRepository : BaseRepository<ProductCategoryDbModel>, IProductCategoryRepository
    {
        public ProductCategoryRepository(SimpleDbContext simpleDbContext) : base(simpleDbContext) { }
    }
}