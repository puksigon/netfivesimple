using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NetFileSimple.Domains.DbModels;

namespace NetFiveSimple.Persistence.EntityConfigs
{
    public class ProductCategoryDbModelEntityConfig : IEntityTypeConfiguration<ProductCategoryDbModel>
    {
        public void Configure(EntityTypeBuilder<ProductCategoryDbModel> builder)
        {
            builder.ToTable("product_category");
            builder.HasKey(model => model.Id);
        }
    }
}