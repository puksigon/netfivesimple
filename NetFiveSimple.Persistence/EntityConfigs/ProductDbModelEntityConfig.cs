using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NetFileSimple.Domains.DbModels;

namespace NetFiveSimple.Persistence.EntityConfigs
{
    public class ProductDbModelEntityConfig : IEntityTypeConfiguration<ProductDbModel>
    {
        public void Configure(EntityTypeBuilder<ProductDbModel> builder)
        {
            builder.ToTable("product");
            builder.HasKey(model => model.Id);

            builder.HasOne(model => model.ProductCategoryDbModel)
                .WithMany(model => model.ProductDbModels)
                .HasForeignKey(model => model.ProductCategoryId);
        }
    }
}