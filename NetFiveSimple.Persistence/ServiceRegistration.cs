using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NetFileSimple.Domains.Interfaces.Repositories;
using NetFiveSimple.Persistence.DbContext;
using NetFiveSimple.Persistence.Repositories;

namespace NetFiveSimple.Persistence
{
    public static class ServiceRegistration
    {
        /// <summary>
        /// Add InmemoryDB driver for SimpleDbContext
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void AddSimpleInmemoryDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<SimpleDbContext>(builder =>
            {
                builder.UseInMemoryDatabase("simple_db");
            });

            services.AddRepositories(configuration);
        }

        /// <summary>
        /// Add Postgre DB driver for SimpleDbContext
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <exception cref="NotImplementedException"></exception>
        public static void AddPgSimpleDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            // services.AddDbContext<SimpleDbContext>(builder =>
            // {
            //     builder.UseInMemoryDatabase("simple_db");
            // });
            //
            // services.AddRepositories(configuration);

            throw new NotImplementedException();
        }
        
        /// <summary>
        /// Add Repositories
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        
        public static void AddRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IProductCategoryRepository, ProductCategoryRepository>();
        }
    }
}