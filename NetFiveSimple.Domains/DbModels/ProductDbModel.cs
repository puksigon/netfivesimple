using System.ComponentModel.DataAnnotations.Schema;

namespace NetFileSimple.Domains.DbModels
{
    public class ProductDbModel
    {
        [Column("id")]
        public int Id { get; set; }
        [Column("product_name")]
        public string ProductName { get; set; }
        
        [Column("product_category_id")]
        public int ProductCategoryId { get; set; }

        public virtual ProductCategoryDbModel ProductCategoryDbModel { get; set; }
    }
}