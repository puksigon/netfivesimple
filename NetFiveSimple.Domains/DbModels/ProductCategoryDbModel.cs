using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace NetFileSimple.Domains.DbModels
{
    public class ProductCategoryDbModel
    {
        [Column("id")]
        public int Id { get; set; }
        [Column("product_category_name")]
        public string ProductCategoryName { get; set; }
        public virtual ICollection<ProductDbModel> ProductDbModels { get; set; }
    }
}