namespace NetFileSimple.Domains.DTOs
{
    public class ResponseProductDto
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
    }
}