namespace NetFileSimple.Domains.DTOs
{
    public class InsertProductCategoryDto
    {
        public string ProductCategoryName { get; set; }
    }
}