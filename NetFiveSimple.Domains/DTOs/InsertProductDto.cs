namespace NetFileSimple.Domains.DTOs
{
    public class InsertProductDto
    {
        public string ProductName { get; set; }
        public int ProductCategoryId { get; set; }
    }
}