using NetFileSimple.Domains.DbModels;

namespace NetFileSimple.Domains.Interfaces.Repositories
{
    public interface IProductRepository : IBaseRepository<ProductDbModel> { }
}