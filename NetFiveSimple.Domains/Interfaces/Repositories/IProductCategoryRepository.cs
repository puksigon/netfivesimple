using NetFileSimple.Domains.DbModels;

namespace NetFileSimple.Domains.Interfaces.Repositories
{
    public interface IProductCategoryRepository : IBaseRepository<ProductCategoryDbModel> { }
}